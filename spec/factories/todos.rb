# frozen_string_literal: true

FactoryBot.define do
  factory :todo do
    title { Faker::Games::Witcher.character }
    created_by { Faker::Number.number(3) }
  end
end
