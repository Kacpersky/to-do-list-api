# frozen_string_literal: true

FactoryBot.define do
  factory :item do
    name { Faker::Games::Witcher.monster }
    done { false }
    todo_id { nil }
  end
end
